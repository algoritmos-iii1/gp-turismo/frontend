new Vue({
    el: "#app",
    data() {
        return {
            viagens: null,
            viagem: {
				idViagem: null,
                cidadePartida: null,
                cidadeChegada: null,
                horaChegada: null,
                horaPartida: null,
                valor: null,
                tipoViagem: null,
				idExcursao: null,
				idVeiculo: null,
            },

        };
    },

    mounted() {
        this.listar();
    },

    methods: {
        listar() {
            axios.get("http://127.0.0.1:5000/viagens").then((response) => (this.viagens = response.data));
        }
    },
    salvar() {
        if (this.viagem.idViagem) {
            axios
                .put(`http://127.0.0.1:5000/viagens/${this.viagem.idViagem}`, this.viagem)
                .then((response) => {
                    alert(`Ok! Viagem Alterado com Sucesso!!`);
                    this.listar();
                });
        } else {
            axios.post("http://127.0.0.1:5000/viagens", this.viagem).then((response) => {
                alert(`Ok! Viagem Cadastrado com Código: ${response.data.id}`);
                this.listar();
            });
        }
        this.viagem = {};
    },

    editar(id) {
        axios
            .get("http://127.0.0.1:5000/viagens/" + id)
            .then((response) => (this.viagens = response.data));
        this.$refs.destino.focus();
    },

    excluir(id, destino) {
        if (confirm(`Confirma exclusão do viagem '${destino}'?`)) {
          axios.delete("http://127.0.0.1:5000/viagens/" + id).then((response) => {
            alert(`Ok! Viagem parada '${destino}' excluída com sucesso!`);
            this.listar();
          });
        }
      },


})